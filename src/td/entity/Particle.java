package td.entity;

import java.util.Random;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import td.Game;
import td.level.Level;

public class Particle extends Entity {
	private double xa, ya, base_ya;
	public int stopTime;
	
	public Particle(double x, double y, Level level, int row, BufferedImage image) {
		super(x, y, null, level, row);
		Random random = new Random();
		this.image = image;
		base_ya = random.nextInt(6) + 6;
		ya = -base_ya;
		xa = random.nextInt(4) - 2;
		stopTime = random.nextInt(10) + 10;
	}
	
	public void die() {
		level.removeEntity(row, this);
	}
	
	@Override
	public void tick() {
		if(stopTime > 0)
			stopTime--;
		if(stopTime <= 0 || y > Game.HEIGHT - image.getHeight())
			die();
		if(ya < base_ya)
			ya++;
		if(ya >= base_ya)
			ya = ya * 1.2;
		this.y += ya;
		this.x += xa;
	}
	
	@Override
	public void render(Graphics g) {
		g.drawImage(image, (int) x, (int) y, null);
	}
}
