package td.entity;

import td.gfx.Gfx;
import td.level.Level;

public class LawnMower extends Entity {
	private boolean mowing;
	
	public LawnMower(double x, double y, Level level, int row) {
		super(x, y, Gfx.lawn_mower, level, row);
	}
	
	@Override
	public void tick() {
		if(mowing) {
			x += 5;
			if(x >= 704)
				level.removeEntity(row, this);
		}
		for(int i = 0; i < level.entities[row].size(); i++) {
			Entity e = level.entities[row].get(i);
			if(!(e instanceof Enemy))
				continue;
			if(e.x <= 32)
				mowing = true;
			if(mowing && e.x >= x + 8 && e.x <= x + 16)
				((Enemy) e).kill();
		}
	}
}
