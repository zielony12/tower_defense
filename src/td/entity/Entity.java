package td.entity;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import td.level.Level;

public class Entity {
	public double x;
	public double y;
	public BufferedImage image;
	protected Level level;
	public int row;
	
	public Entity(double x, double y, BufferedImage image, Level level, int row) {
		this.x = x;
		this.y = y;
		this.image = image;
		this.level = level;
		this.row = row;
	}
	
	public void render(Graphics g) {
		g.drawImage(image, (int) x, (int) y, null);
	}
	
	public void tick() {
	}
}
