package td.entity;

import java.awt.image.BufferedImage;

import td.Game;
import td.level.Level;

public class Bullet extends Entity {
	public boolean alive = true;
	
	public Bullet(double x, double y, Level level, int row, BufferedImage image) {
		super(x, y, image, level, row);
	}
	
	@Override
	public void tick() {
		for(int i = 0; i < level.entities[row].size(); i++) {
			Entity e = level.entities[row].get(i);
			if(!(e instanceof Enemy))
				continue;
			if(x >= e.x && x <= e.x + 64) {
				level.removeEntity(row, this);
				alive = false;
				((Enemy) e).hurt(this);
			}
		}
		
		if(x >= Game.WIDTH)
			level.removeEntity(row, this);
		this.x += 5;
	}
}
