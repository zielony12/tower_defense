package td.entity;

import td.gfx.Gfx;
import td.level.Level;

public class SunEntity extends Entity {
	private double dirX, dirY;
	private int flyEndX, flyEndY;
	private boolean bX;
	
	public SunEntity(double x, double y, double dirX, double dirY, int flyEndX, int flyEndY, Level level) {
		super(x, y, Gfx.sun, level, 0);
		this.dirX = dirX;
		this.dirY = dirY;
		this.flyEndX = flyEndX;
		this.flyEndY = flyEndY;
		
		if(x < flyEndX)
			bX = true;
		else
			bX = false;
	}
	
	@Override
	public void tick() {
		if((bX && ((int) x >= flyEndX)) || (!bX && ((int) x <= flyEndX )) && (int) y >= flyEndY) {
			level.sunsBalance += 25;
			level.removeEntity(0, this);
		}
		x += dirX;
		y += dirY;
	}
}
