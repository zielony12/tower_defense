package td.entity;

import java.awt.Graphics;

import td.level.Level;
import td.gfx.Gfx;

public class Zombie extends Enemy {
	private int animTimer;
	private int animTimerMax = 15;
	private int bitmap_index = 0;
	private int bitmap_index_dir = 1;
	
	public Zombie(double x, double y, Level level, int row) {
		super(x, y, Gfx.zombie[0], level, row, 3);
	}
	
	@Override
	public void render(Graphics g) {
		super.render(g);
		
		if(animTimer > 0)
			animTimer--;
		else {
			animTimer = animTimerMax;
			
			if(bitmap_index >= Gfx.zombie.length - 1)
				bitmap_index_dir = -1;
			if(bitmap_index <= 0)
				bitmap_index_dir = 1;
			
			bitmap_index += bitmap_index_dir;
			image = Gfx.zombie[bitmap_index];
		}
	}
}
