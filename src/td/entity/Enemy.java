package td.entity;

import java.util.Random;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import td.level.Level;
import td.gfx.Gfx;
import td.level.Field;

public class Enemy extends Entity {
	public double speed;
	public double xa;
	private int hurtTimer;
	private int hurtTimerMax = 30;
	protected int maxHealth;
	private int health;
	protected Random random = new Random();
	
	public Enemy(double x, double y, BufferedImage image, Level level, int row, int maxHealth) {
		super(x, y, image, level, row);
		this.maxHealth = maxHealth;
		health = maxHealth;
		double baseSpeed = 0.05 + random.nextDouble() * 0.25;
	    speed = -baseSpeed - random.nextDouble() * 0.3;

	    if(level.wave > 0) {
	        speed *= level.wave * 1.5;
	    }

		xa = speed;
	}
	
	@Override
	public void tick() {
		if(x < 0 - image.getWidth())
			level.removeEntity(row, this);
		x += xa;
		if(x <= -64)
			level.lose();
		
		for(int i = 0; i < level.lawn[row].length; i++) {
			Field f = level.lawn[row][i];
			if(f.placeable != null && x > f.placeable.x && x < f.placeable.x + 32) {
				xa = 0;
				if(hurtTimer > 0)
					hurtTimer--;
				else {
					hurtTimer = hurtTimerMax;
					f.placeable.hurt(this);
				}
			}
		}
	}
	
	@Override
	public void render(Graphics g) {
		g.drawImage(image, (int) x, (int) y - 10, null);
	}
	
	public void kill() {
		level.removeEntity(row, this);
	}
	
	public void hurt(Entity e) {
		health--;
		for(int i = random.nextInt(10) + 5; i > 0; i--) {
			level.insertEntity(row, new Particle(x + 8, y + random.nextInt(8) + 8, level, row, Gfx.blood));
		}
		if(health <= 0)
			kill();
	}
}
