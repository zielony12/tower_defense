package td.entity;

import td.level.Level;
import td.gfx.Gfx;

public class Pacman extends Enemy {
	private int animTimer;
	private static int animTimerMax = 5;
	private int bitmap_index = 0;
	private int bitmap_index_dir = 1;
	
	public Pacman(double x, double y, Level level, int row) {
		super(x, y, Gfx.pacman[0], level, row, 5);
	}

	@Override
	public void tick() {
		super.tick();

		if(animTimer > 0)
			animTimer--;
		else {
			animTimer = animTimerMax;
			
			if(bitmap_index >= Gfx.pacman.length - 1)
				bitmap_index_dir = -1;
			if(bitmap_index <= 0)
				bitmap_index_dir = 1;
			
			bitmap_index += bitmap_index_dir;
			image = Gfx.pacman[bitmap_index];
		}
	}
	
	@Override
	public void hurt(Entity e) {
		super.hurt(e);
	}
}
