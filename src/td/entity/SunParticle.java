package td.entity;

import td.level.Level;
import td.gfx.Gfx;
import td.gui.Sun;

public class SunParticle extends Particle {
	public SunParticle(double x, double y, Level level, int row) {
		super(x, y, level, row, Gfx.sun); 
	}
	
	@Override
	public void die() {
		super.die();
		level.suns.add(new Sun((int) x, (int) y, level));
	}
}
