package td.entity;

import java.awt.image.BufferedImage;

import td.level.Level;

public class Pea extends Bullet {
	private BufferedImage[] images;
	private int bitmap_index;
	private static int animTimerMax = 5;
	private int animTimer = animTimerMax;
	
	public Pea(double x, double y, Level level, int row, BufferedImage[] images) {
		super(x, y, level, row, images[0]);
		this.images = images;
	}
	
	@Override
	public void tick() {
		super.tick();
		
		if(animTimer > 0)
			animTimer--;
		else {
			animTimer = animTimerMax;
			bitmap_index++;
			
			if(bitmap_index > 1)
				bitmap_index = 0;
			
			image = images[bitmap_index];
		}
	}
}
