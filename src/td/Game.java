package td;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferStrategy;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;

import javax.swing.JFrame;

import td.level.Level;
import td.menu.Menu;
import td.menu.MainMenu;

public class Game extends Canvas implements Runnable {
	// Game content scale in percents from base 640x480 ({3, 2.25} for 1920x1080)
	public static double SCALEX;
	public static double SCALEY;
	
	// Window dimensions in pixels
	public static int WIDTH;
	public static int HEIGHT;
	private static Dimension dimension;
	
	private boolean running = false;
	
	private int gcTimer;
	
	private InputHandler input;
	public Level level;
	
	public int plantSlots = 3;
	
	public Menu menu;
		
	public Game() {
		input = new InputHandler(this);
		menu = new MainMenu(null, this);
	}
	
	private void tick() {
		if(menu != null)
			menu.tick();
		else
			level.tick();
		
		if(gcTimer >= 4200) {
			gcTimer = 0;
			System.out.println("Time for tidying");
			System.gc();
		}
	}
	
	private void render() {
		BufferStrategy bs = getBufferStrategy();
		if (bs == null) {
			createBufferStrategy(2);
			requestFocus();
			return;
		}
		
		Graphics g = bs.getDrawGraphics();
		g.setColor(new Color(255));
		g.fillRect(0, 0, getWidth(), getHeight());
		((Graphics2D) g).scale(SCALEX, SCALEY);
		
		if(menu != null)
			menu.render(g);
		else
			level.render(g);
		
		g.dispose();
		bs.show();
	}
	
	public void run() {
	    while (running) {
	    	// check for input events asap
	    	input.tick();
	    	
	    	long lastTime = System.nanoTime();
			double unprocessed = 0;
			double nsPerTick = 1000000000.0 / 60;
			int ticks = 0;
			long lastTimer1 = System.currentTimeMillis();

			while (running) {
				long now = System.nanoTime();
				unprocessed += (now - lastTime) / nsPerTick;
				lastTime = now;
				while (unprocessed >= 1) {
					ticks++;
					tick();
					render();
					unprocessed -= 1;
				}

				try {
					Thread.sleep(2);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				if (System.currentTimeMillis() - lastTimer1 > 1000) {
					lastTimer1 += 1000;
					System.out.println(ticks + " ticks");
					ticks = 0;
				}
			}
	    }
	}
	
	public void start() {
		running = true;
		new Thread(this).start();
	}

	public void clicked(int x, int y) {
		System.out.println("X: " + x + "; Y: " + y);
		if(menu != null)
			menu.clicked(x, y);
		else
			level.clicked(x, y);
	}

	public void moved(int x, int y) {
		if(menu != null)
			menu.moved(x, y);
		else
			level.moved(x, y);
	}
	
	static {
		dimension = Toolkit.getDefaultToolkit().getScreenSize();
		WIDTH = (int) dimension.getWidth();
		HEIGHT = (int) dimension.getHeight();
		
		SCALEX = (double) WIDTH / 640;
		SCALEY = (double) HEIGHT / 480;
	}
	
	public static void main(String[] args) {
		// Hardware acceleration
		System.setProperty("sun.java2d.opengl", "True");
		
		// TODO: fullscreen with original (4:3) aspect ratio
		Game game = new Game();
		game.setPreferredSize(dimension);
		game.setMinimumSize(dimension);
		game.setMaximumSize(dimension);
		game.setSize(dimension);
		game.setVisible(true);
		
		JFrame frame = new JFrame("Tower defense");
		frame.add(game);
		
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
		frame.setUndecorated(true);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		game.start();
	}
}
