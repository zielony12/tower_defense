package td.menu;

import java.awt.Graphics;

import td.gui.Button;
import td.Game;

public class Menu {
	protected Button[] options;
	protected Game game;
	private Menu parent;
	
	public Menu(Menu parent, Game game) {
		this.parent = parent;
		this.game = game;
		game.menu = this;
	}
	
	public void tick() {
	}
	
	public void render(Graphics g) {
		for(int i = 0; i < options.length; i++) {
			options[i].render(g);
		}
	}
	
	public void clicked(int x, int y) {
		for(int i = 0; i < options.length; i++) {
			if(options[i].checkPoint(x, y))
				select(i);
		}
	}
	
	protected void select(int i) {
	}
	
	public void moved(int x, int y) {
		for(int i = 0; i < options.length; i++) {
			if(options[i].checkPoint(x, y))
				options[i].hovered = true;
			else
				options[i].hovered = false;
		}
	}
}
