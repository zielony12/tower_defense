package td.menu;

import java.awt.Graphics;

import td.Game;
import td.gui.Button;
import td.gfx.Gfx;
import td.gui.ImageButton;
import td.level.BasicLevel;

public class MainMenu extends Menu {
	private double ya = 1.1;
	private double y = -200;
	
	public MainMenu(Menu parent, Game game) {
		super(parent, game);
		
		options = new Button[] {
				new ImageButton(350, 100, 250, 150, Gfx.play_btn),
		};
	}
	
	@Override
	public void tick() {
		super.tick();
		if(y >= 10)
			return;
		else
			y += ya;
		
		ya *= 1.1;
	}
	
	@Override
	public void render(Graphics g) {
		g.drawImage(Gfx.title, 0, 0, null);
		g.drawImage(Gfx.logo, 0, (int) y, null);
		super.render(g);
	}
	
	@Override
	public void select(int i) {
		switch(i) {
		case 0:
			game.level = new BasicLevel();
			game.menu = null;
			break;
		}
	}
}
