package td.placeable;

import java.util.Random;

import td.gfx.Gfx;
import td.level.Level;

public class Wallnut extends Placeable {
	private Random random = new Random();
	private int bitmap_index = 0;
	
	public Wallnut(int x, int y, Level level, int row, int col) {
		super(x, y, Gfx.wallnut[0], level, row, col, 0x8A6E56, 40);
	}

	@Override
	public void tick() {
		int tmp_index = (int) Math.round(((double) health / (double) maxHealth) / 0.25);
		
		// Reverse index; From 3->2->1->0 to 1->2->3->4.
		int offset = 3;
		int bitmap_index = offset - tmp_index;
		if(bitmap_index < 0)
			bitmap_index = 0;
		else if(bitmap_index > offset)
			bitmap_index = offset;
		
		image = Gfx.wallnut[bitmap_index];
	}
}
