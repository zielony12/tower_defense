package td.placeable;

import java.awt.image.BufferedImage;

import td.gfx.Gfx;

public enum Blueprint {	
	SUNFLOWER(50, 300, Sunflower.class, Gfx.sunflower),
	PEASHOOTER(100, 600, Peashooter.class, Gfx.peashooter),
	WALLNUT(75, 1200, Wallnut.class, Gfx.wallnut[0]);
	
	Blueprint(int cost, int cooldown, Class clazz, BufferedImage icon) {
		this.cost = cost;
		this.cooldown = cooldown;
		this.clazz = clazz;
		this.icon = icon;
	}
	
	public int cost;
	public int cooldown;
	public Class clazz;
	public BufferedImage icon;
}
