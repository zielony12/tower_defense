package td.placeable;

import td.gfx.Gfx;
import td.entity.Pea;
import td.entity.Enemy;
import td.entity.Entity;
import td.level.Level;

public class Peashooter extends Placeable {
	private static final int shootTimerMax = 90;
	private int shootTimer = shootTimerMax;
	
	public Peashooter(int x, int y, Level level, int row, int col) {
		super(x, y, Gfx.peashooter, level, row, col, 0xAAFFAA, 5);
	}
	
	@Override
	public void tick() {
		boolean entitiesInRow = false;
		int enemyCounter = 0;
		
		for(int i = 0; i < level.entities[row].size(); i++) {
			Entity e = level.entities[row].get(i);
			if(e instanceof Enemy && e.x > x)
				enemyCounter++;
			if(enemyCounter > 0)
				entitiesInRow = true;
			else
				entitiesInRow = false;
		}
		
		if(entitiesInRow) {
			if(shootTimer > 0)
				shootTimer--;
			else {
				shootTimer = shootTimerMax;
				level.insertEntity(row, new Pea((double) x + 48, (double) y + 13, level, row, Gfx.pea));
			}
		}
	}
}
