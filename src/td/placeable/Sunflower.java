package td.placeable;

import java.util.Random;

import td.gfx.Gfx;
import td.level.Level;
import td.entity.SunParticle;

public class Sunflower extends Placeable {
	private Random random = new Random();
	private int sunTimer = 500;

	public Sunflower(int x, int y, Level level, int row, int col) {
		super(x, y, Gfx.sunflower, level, row, col, 0xAAAA00, 5);
	}
	
	@Override
	public void tick() {
		if(sunTimer > 0)
			sunTimer--;
		else {
			sunTimer = 500;
//			level.suns.add(new Sun(x + (random.nextInt(32)), y + (random.nextInt(32)), level));
			level.insertEntity(row, new SunParticle(x + random.nextInt(32), y + random.nextInt(32), level, row));
		}
	}
}
