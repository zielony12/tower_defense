package td.placeable;

import java.util.Random;
import java.util.List;
import java.util.ArrayList;
import java.awt.image.BufferedImage;
import java.awt.Graphics;

import td.entity.Particle;
import td.entity.Enemy;
import td.entity.Entity;
import td.gfx.Gfx;
import td.level.Level;

public class Placeable {
	public int x;
	public int y;
	public BufferedImage image;
	protected Level level;
	protected int row;
	protected int col;
	protected int health;
	protected int maxHealth;
	protected int color;
	private Random random = new Random();
	private List<Enemy> attackers = new ArrayList<Enemy>();
	
	public Placeable(int x, int y, BufferedImage image, Level level, int row, int col, int color, int health) {
		this.x = x;
		this.y = y;
		this.image = image;
		this.level = level;
		this.row = row;
		this.col = col;
		this.color = color;
		this.maxHealth = health;
		this.health = maxHealth;
	}
	
	public void hurt(Entity entity) {
		if(entity instanceof Enemy)
			attackers.add((Enemy) entity);
		health--;
		for(int i = random.nextInt(8) + 4; i > 0; i--) {
			level.insertEntity(row, new Particle((double) x + 32, (double) y + random.nextInt(10) + 10, level, row, Gfx.dim(Gfx.shards[random.nextInt(2)], color)));
		}
		if(health <= 0) {
			kill();
		}
	}
	
	public void kill() {
		while(attackers.size() > 0) {
			Enemy attacker = attackers.get(0);
			attacker.xa = attacker.speed;
			attackers.remove(attacker);
		}
		level.lawn[row][col].placeable = null;
	}
	
	public void render(Graphics g) {
		g.drawImage(image, x, y, null);
	}
	
	public void tick() {
	}
}
