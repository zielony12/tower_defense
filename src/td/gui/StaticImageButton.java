package td.gui;

import java.awt.image.BufferedImage;
import java.awt.Graphics;

import td.gfx.Gfx;

public class StaticImageButton extends Button {
	protected BufferedImage[] images;
	
	public StaticImageButton(int x, int y, int width, int height, BufferedImage[] images) {
		super(x, y, width, height, 0);
		this.images = images;
	}
	
	@Override
	public void render(Graphics g) {
		BufferedImage image = (disabled ? images[1] : images[0]);
		g.drawImage((hovered && !disabled ? Gfx.dim(image) : image), x, y, width, height, null);
	}
}
