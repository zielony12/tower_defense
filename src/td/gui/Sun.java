package td.gui;

import java.awt.Graphics;

import td.level.Level;
import td.gfx.Gfx;
import td.entity.SunEntity;

public class Sun extends Button {
	private Level level;
	
	public Sun(int x, int y, Level level) {
		super(x, y, 32, 32, 0xFFFFFF);
		this.level = level;
	}
	
	@Override
	public void callback() {
		int dx = 420 - x;
		int dy = 430 - y;
		double len = Math.sqrt(dx * dx + dy * dy);
		double dirX = dx / len;
		double dirY = dy / len;
		
		level.insertEntity(0, new SunEntity(x, y, dirX * 5, dirY * 5, 420, 430, level));
		level.suns.remove(this);
	}
	
	@Override
	public void render(Graphics g) {
		g.drawImage((hovered ? Gfx.dim(Gfx.sun) : Gfx.sun), x, y, null);
	}
}
