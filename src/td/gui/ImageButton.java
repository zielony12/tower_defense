package td.gui;

import java.awt.image.BufferedImage;
import java.awt.Graphics;

public class ImageButton extends Button {
	BufferedImage[] images;
	
	public ImageButton(int x, int y, int width, int height, BufferedImage[] images) {
		super(x, y, width, height, 0);
		this.images = images;
	}
	
	@Override
	public void render(Graphics g) {
		g.drawImage(images[ hovered ? 1 : 0], x, y, width, height, null);
	}
}
