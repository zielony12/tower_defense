package td.gui;

import java.util.List;
import java.util.ArrayList;
import java.awt.Graphics;
import java.awt.Color;

import td.gfx.Gfx;
import td.placeable.Blueprint;

public class PlantButton extends Button {
	private int id;
	public static int selected_id;
	public Blueprint blueprint;
	public int cooldown;
	
	public static List<PlantButton> plantButtons = new ArrayList<PlantButton>();

	public PlantButton(int id, Blueprint blueprint, int x, int y) {
		super(x, y, 64, 64, 0x696969);
		this.id = id;
		this.blueprint = blueprint;
		
		selected_id = -1;		
		plantButtons.add(this);
	}

	public void tick() {
		if(cooldown > 0)
			cooldown--;
	}

	@Override
	public void render(Graphics g) {		
		if(blueprint != null) {
			g.drawImage((hovered ? Gfx.dim(blueprint.icon) : blueprint.icon), x, y, null);
		
			g.setColor(new Color(255, 255, 255, 128));
			int height = (int) (((double) cooldown / (double) blueprint.cooldown) * 64);
			g.fillRect(x, y - height + 64, 64, height);
			
			g.drawImage(Gfx.sun, x, y - 4, 16, 16, null);
			g.setFont(Gfx.font_xs);
			g.setColor(Color.WHITE);
			g.drawString("" + blueprint.cost, x + 16, y + 10);
			if(hovered)
				g.drawString(blueprint.clazz.getSimpleName(), x, y + 64);
		}
		
		if(selected_id == id)
			g.drawImage(Gfx.selection_overlay, x, y, null);
	}

	@Override
	public void callback() {
		if(selected_id == id) {
			selected_id = -1;
			return;
		}
		
		selected_id = id;
	}
}
