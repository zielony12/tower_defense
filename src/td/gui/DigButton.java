package td.gui;

import java.awt.Graphics;

import td.gfx.Gfx;

public class DigButton extends PlantButton {
	public DigButton(int id, int x, int y) {
		super(id, null, x, y);
	}
	
	@Override
	public void tick() {
	}
	
	@Override
	public void render(Graphics g) {
		g.drawImage(Gfx.shovel, x, y, null);
		super.render(g);
	}
}
