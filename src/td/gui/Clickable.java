package td.gui;

import td.Game;

public class Clickable {
	protected int x;
	protected int y;
	protected int width;
	protected int height;

	public Clickable(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public boolean checkPoint(int x, int y) {
		if(x / Game.SCALEX > this.x && y / Game.SCALEY > this.y && x / Game.SCALEX <= this.x + this.width && y / Game.SCALEY <= this.y + this.height)
			return true;
		return false;
	}
}
