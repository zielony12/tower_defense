package td.gui;

import java.awt.Graphics;
import java.awt.Color;

import td.gfx.Gfx;

public class TextButton extends Button {
	private String text;
	
	public TextButton(int x, int y, int width, int height, String text) {
		super(x, y, width, height, 0xBABABA);
		this.text = text;
	}
	
	@Override
	public void render(Graphics g) {
		super.render(g);
		g.setColor(Color.white);
		g.setFont(Gfx.font_s);
		g.drawString(text, x + (width / 2) - ((text.length() * 10) / 2), y + 25);
	}
}
