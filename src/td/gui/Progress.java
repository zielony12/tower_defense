package td.gui;

import java.awt.Graphics;
import java.awt.Color;

public class Progress {
	public int progress;
	public int maxProgress;
	private int x, y;
	
	public Progress(int x, int y, int maxProgress) {
		this.x = x;
		this.y = y;
		
		this.maxProgress = maxProgress;
	}
	
	public void render(Graphics g) {
		Color color1 = new Color(0xBABABA);
	    Color color2 = new Color(0xBAFFBA);
	    
	    g.setColor(color1);
	    g.fillRect(this.x, this.y, 128, 16);

	    g.setColor(color2);
	    int progressWidth = (int) ((((double) progress / (double) maxProgress) * 128));
	    g.fillRect(this.x, this.y, progressWidth, 16);
	    
		//Bitmap.draw(pixels, Bitmap.warning, x, y, 0xFFFFFF);
	}
}
