package td.gui;

import java.awt.Graphics;
import java.awt.Color;

public class Button extends Clickable {
	public boolean hovered;
	public boolean disabled;
	protected int color;

	public Button(int x, int y, int width, int height, int color) {
		super(x, y, width, height);
		this.color = color;
	}
	
	public void render(Graphics g) {
		g.setColor(new Color((disabled ? 0x211212 : (hovered ? color & 0xABABAB : color))));
		g.fillRect(x, y, width, height);
	}

	public void callback() {
	}
}
