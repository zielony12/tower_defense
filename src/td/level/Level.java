package td.level;

import java.util.List;
import java.util.ArrayList;
import java.util.Random;
import java.awt.Graphics;
import java.awt.Color;

import td.entity.Entity;
import td.gfx.Gfx;
import td.entity.Enemy;
import td.Game;
import td.gui.Progress;
import td.gui.Sun;
import td.gui.PlantButton;

public class Level {
	protected Random random = new Random();
	
    public Field[][] lawn = new Field[5][9];
    
    public List<PlantButton> bar;
    public List<Sun> suns = new ArrayList<Sun>();
    public List<Entity>[] entities = new ArrayList[5];
    
    public int sunsBalance = 50;
    public int wave;
    
    public boolean showPreview;
    private double previewX;
    private double previewY;
    
    protected boolean won;
    
    public Level() {
        for(int y = 0; y < 5; y++) {
            for(int x = 0; x < 9; x++) {
                lawn[y][x] = new Field((64 + x * 64), y * 64 + 64, 64, 64, this, y, x);
            }
        }
        
        for(int i = 0; i < 5; i++) {
        	entities[i] = new ArrayList<Entity>();
        }
    }

    public void removeEntity(int row, Entity entity) {
    	entities[row].remove(entity);
    }
    
    public void insertEntity(int row, Entity entity) {
    	entities[row].add(entity);
    }
    
    public void render(Graphics g) {
    	g.drawImage(Gfx.bg_day, 0, 0, null);
    	for(int y = 0; y < 5; y++) {
            for(int x = 0; x < 9; x++) {
            	if(y == 0)
        			g.drawImage(Gfx.dim(Gfx.seam[0], (lawn[y][x].disabled ? 0x6d493b : 0x2fb330)), 64 + x * 64, 60, null);
        		if(y == 4)
        			g.drawImage(Gfx.dim(Gfx.seam[1], (lawn[y][x].disabled ? 0x6d493b : 0x2fb330)), 64 + x * 64, 64 + y * 64 + 64, null);
        		lawn[y][x].render(g);
            }
        }
    	
    	g.setColor(Color.BLACK);
    	g.fillRect(0, 400, 640, 80);
    	
    	for(int i = 0; i < bar.size(); i++) {
    		bar.get(i).render(g);
    	}
    	
    	for(int i = 0; i < entities.length; i++) {
        	for(int j = 0; j < entities[i].size(); j++) {
        		entities[i].get(j).render(g);
        	}
        }
    	
    	for(int i = 0; i < suns.size(); i++) {
    		suns.get(i).render(g);
    	}
        
        /*for(int y = 0; y < 32; y++) {
    /    	for(int x = 0; x < 32; x++) {
        		int color = Bitmap.sun.pixels[x + y * 32];
        		if(color != 0xFFFF00FF)
        			pixels[(x + 370) + (y + 420) * Game.WIDTH] = color;
        	}
        }*/

        //Bitmap.drawText(g, 400, 400, Game.WIDTH, Game.HEIGHT, suns + "", 0xFFFFFF);
        g.drawImage(Gfx.sun, 420, 430, null);
        g.setColor(new Color(0xFFFFFF));
        g.setFont(Gfx.font);
        g.drawString("" + sunsBalance, 460, 460);
        
        if(showPreview && PlantButton.selected_id != -1)
        	g.drawImage((PlantButton.plantButtons.get(PlantButton.selected_id).blueprint == null ? Gfx.shovel : PlantButton.plantButtons.get(PlantButton.selected_id).blueprint.icon), (int) previewX, (int) previewY, null);
    }

    public void tick() {
    	// TODO: add reward at the end of the level

	if(won) {
    		return;
    	}
    	
    	for(int y = 0; y < 5; y++) {
            for(int x = 0; x < 9; x++) { 
        		lawn[y][x].tick();
            }
        }
    	
    	for(int i = 0; i < bar.size(); i++) {
    		bar.get(i).tick();
    	}
    	
    	for(int i = 0; i < entities.length; i++) {
        	for(int j = 0; j < entities[i].size(); j++) {
        		entities[i].get(j).tick();
        	}
        }
    }
    
    public void lose() {
    	System.out.println("GAME OVER");
    }

    public void finish() {
        System.out.println("FINISHED");
        won = true;        
    }
    
    public void clicked(int x, int y) {
    	for(int yy = 0; yy < 5; yy++) {
            for(int xx = 0; xx < 9; xx++) { 
        		if(lawn[yy][xx].checkPoint(x, y))
            		lawn[yy][xx].callback();
            }
        }
    	
    	for(int i = 0; i < bar.size(); i++) {
    		if(bar.get(i).checkPoint(x, y))
    			bar.get(i).callback();
    	}
    	
    	for(int i = 0; i < suns.size(); i++) {
    		if(suns.get(i).checkPoint(x, y))
    			suns.get(i).callback();
    	}
    }
    
    public void moved(int x, int y) {
    	if(PlantButton.selected_id != -1) {
    		previewX = (x / Game.SCALEX) - 32;
    		previewY = (y / Game.SCALEY) - 32;
    		showPreview = true;
    	}
    	for(int yy = 0; yy < 5; yy++) {
            for(int xx = 0; xx < 9; xx++) { 
        		if(lawn[yy][xx].checkPoint(x, y)) {
            		lawn[yy][xx].hovered = true;
        		} else
        			lawn[yy][xx].hovered = false;
            }
            
            for(int i = 0; i < bar.size(); i++) {
        		if(bar.get(i).checkPoint(x, y))
        			bar.get(i).hovered = true;
        		else
        			bar.get(i).hovered = false;
        	}
            
            for(int i = 0; i < suns.size(); i++) {
        		if(suns.get(i).checkPoint(x, y))
        			suns.get(i).hovered = true;
        		else
        			suns.get(i).hovered = false;
        	}
        }
    }
}
