package td.level;

import td.entity.Enemy;

public class EnemyBlueprint {
	public Class<? extends Enemy> clazz;
	public double spawnChance;
	
	public EnemyBlueprint(Class<? extends Enemy> clazz, double spawnChance) {
		this.clazz = clazz;
		this.spawnChance = spawnChance;
	}
}