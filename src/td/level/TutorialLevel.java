/*package td.level;

import java.util.ArrayList;

import td.gui.PlantButton;
import td.gui.Progress;
import td.placeable.Blueprint;
import td.entity.Zombie;

// TODO: tutorial text
public class TutorialLevel extends Level {
	private boolean atLeastOne;
	private int enemySpawnTimer = 60;
	private int[] enemySpawnBounds = {1, 4};
	
    public TutorialLevel() {
    	super();
    	
    	for(int i = 0; i < 5; i++) {
    		if(i == 2)
    			continue;
    		for(int j = 0; j < 9; j++) {
    			lawn[i][j].disabled = true;
    		}
    	}
    	
    	sunsBalance = 100;
        
    	bar = new ArrayList<PlantButton>();
    	bar.add(new PlantButton(0, Blueprint.PEASHOOTER, 0, 400));

		totalWaves = 3;
		enemiesPerWave = new int[] {2, 3, 6};
		enemiesToSpawn = enemiesPerWave[wave];

		progress = new Progress(512, 0, enemiesPerWave[wave]);
    }
    
    @Override
    public void tick() {
    	super.tick();

    	for(int i = 0; i < bar.size(); i++) {
			bar.get(i).tick();
		}
    	
    	if(atLeastOne && enemySpawnTimer > 0)
    		enemySpawnTimer--;
    	else if(atLeastOne) {
    		if(progress.progress == progress.maxProgress)
    			enemySpawnTimer = 800;
    		else
    			enemySpawnTimer = 400;
			if(enemiesToSpawn > 0)
    			spawnEnemy();
		}

		if(waveJustStarted) {
			if(wave == 2) {
				enemySpawnBounds[0]--;
				enemySpawnBounds[1]++;
				bar.add(new PlantButton(1, Blueprint.SUNFLOWER, 65, 400));
			}
			if(wave >= 3)
				finish();
			if(wave < 2)
				sunsBalance += wave * 200;
			else
				sunsBalance += 50;
			waveJustStarted = false;
			for(int i = 0; i < 5; i++) {
				if(wave == 1 && (i == 0 || i == 4))
					continue;
				for(int j = 0; j < 9; j++) {
					lawn[i][j].disabled = false;
				}
    		}
		}
    	
    	if(atLeastOne)
    		return;
    	
    	for(int i = 0; i < 9; i++) {
    		if(lawn[2][i].placeable != null) {
    			atLeastOne = true;
    			break;
    		}
    	}
    }
    
    public void spawnEnemy() {
		int row = 2;
		if(wave > 0)
			row = random.nextInt(enemySpawnBounds[1] - enemySpawnBounds[0]) + enemySpawnBounds[0];
    	insertEntity(row, new Zombie(640.0, (row * 64) + 52, this, row));
    }
}*/