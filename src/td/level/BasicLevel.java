package td.level;

import java.util.List;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.awt.Graphics;

import td.entity.Zombie;
import td.gfx.Gfx;
import td.entity.Enemy;
import td.entity.Entity;
import td.entity.Pacman;
import td.gui.PlantButton;
import td.gui.DigButton;
import td.entity.LawnMower;
import td.gui.Progress;
import td.placeable.Blueprint;

public class BasicLevel extends Level {
	private int enemySpawnTimer = 200;
    private int enemySpawnTimerMax = 1200;
    protected List<EnemyBlueprint> enemyBlueprints = new ArrayList<EnemyBlueprint>();
    
    private int levelStartTime = 2000;
    
    protected boolean waveJustStarted;

    protected int enemiesToSpawn;

    protected int[] enemiesPerWave;

    protected int totalWaves;
        
    protected int waveTimer = 60;
    
    protected Progress progress;
    
    public int difficulty = 1;
    
    public BasicLevel() {
    	super();
    	
    	bar = new ArrayList<PlantButton>();
    	
    	for(int i = 0; i < 3; i++) {
			bar.add(new PlantButton(i, Blueprint.values()[i], i * 65, 410));
		}
    	
    	bar.add(new DigButton(3, 574, 400));
    	
    	for(int i = 0; i < 5; i++) {
    		entities[i].add(new LawnMower(0, (48 + i * 64), this, i));
    	}
    	
    	enemyBlueprints.add(new EnemyBlueprint(Zombie.class, 0.9));
        enemyBlueprints.add(new EnemyBlueprint(Pacman.class, 0.9));

        enemiesPerWave = new int[] {
        		5, 10
        };
        
        enemiesToSpawn = enemiesPerWave[wave];
        
		progress = new Progress(512, 0, enemiesPerWave[0]);
    }
    
    public void spawnEnemy() {
    	int row = random.nextInt(5);
    	double chance = random.nextDouble();
    	double cumulative = 0.0;
    	
    	for(EnemyBlueprint enemyBlueprint : enemyBlueprints) {
    		cumulative += enemyBlueprint.spawnChance;
    		if(chance < cumulative) {
    			Constructor<? extends Enemy> cons;
				try {
					cons = enemyBlueprint.clazz.getConstructor(double.class, double.class, Level.class, int.class);
					insertEntity(row, cons.newInstance(640.0, (row * 64) + 52, this, row));
				} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException 
						| InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return;
    		}
    	}
    }
    
    @Override
    public void tick() {
    	super.tick();
    	
    	if(levelStartTime > 0)
    		return;
    	
    	if(enemySpawnTimer > 0)
    		enemySpawnTimer--;
    	else if(enemiesToSpawn > 0) {
    		enemySpawnTimer = enemySpawnTimerMax;
    		spawnEnemy();
    	}
    	
    	if(waveJustStarted == true)
            waveJustStarted = false;
    	
    	if(progress.progress >= progress.maxProgress) {
    		if(waveTimer > 0) {
    			waveTimer--;
    			return;
    		}
    		if(wave >= enemiesPerWave.length - 1)
    			finish();
    		if(won)
    			return;
    		waveTimer = 240;
    		wave++;
            progress.maxProgress = enemiesPerWave[wave];
    		progress.progress = 0;
            enemiesToSpawn = enemiesPerWave[wave];
            enemySpawnTimerMax = Math.max(200, enemySpawnTimerMax - ((wave + 1) * (enemySpawnTimerMax) / 3));
            System.out.println("Enemy spawn timer max: " + enemySpawnTimerMax);
            waveJustStarted = true;
    	}
    }
    
    @Override
    public void removeEntity(int row, Entity entity) {
    	if(entity instanceof Enemy)
    		progress.progress++;
    	entities[row].remove(entity);
    }
    
    @Override
    public void insertEntity(int row, Entity entity) {
    	if(entity instanceof Enemy)
            enemiesToSpawn--;
    	entities[row].add(entity);
    }
    
    @Override
    public void render(Graphics g) {
    	super.render(g);
    	
    	if(levelStartTime > 0)
    		levelStartTime--;
    	else {
	    	progress.render(g);
	    	
	    	g.setFont(Gfx.font_s);
	        g.drawString("Wave " + wave, 450, 15);
    	}
    }
}
