package td.level;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import td.gui.Button;
import td.gfx.Gfx;
import td.gui.PlantButton;
import td.gui.DigButton;
import td.placeable.Placeable;
import td.placeable.Blueprint;

public class Field extends Button {
	public Placeable placeable;
	public int row;
	public int col;
	private Level level;
	
	public Field(int x, int y, int width, int height, Level level, int row, int col) {
		super(x, y, width, height, 0);
		this.level = level;
		this.row = row;
		this.col = col;
	}
	
	@Override
	public void render(Graphics g) {
		//BufferedImage image = Gfx.field[disabled ? 1 : 0];
		//g.drawImage((!disabled && hovered ? Gfx.brighten(image, 0x222222) : image), x, y, null);
		if(hovered && PlantButton.selected_id != -1 && PlantButton.plantButtons.get(PlantButton.selected_id) instanceof DigButton)
			g.drawImage(Gfx.brighten(Gfx.field[0], 0xFF0000), x, y, null);
		else
			g.drawImage((disabled ? Gfx.field[1] : (row % 2 == 0 ? Gfx.brighten(Gfx.field[0], 0x001100) : Gfx.field[0])), x, y, null);
		if(placeable != null)
			placeable.render(g);
		else if(!disabled && hovered) {
			if(PlantButton.selected_id != -1 && PlantButton.plantButtons.get(PlantButton.selected_id).blueprint != null) {
				g.drawImage(Gfx.addTransparency(PlantButton.plantButtons.get(PlantButton.selected_id).blueprint.icon, 192), x, y - 20, null);
			}
		}
	}
	
	public void tick() {
		if(placeable != null)
			placeable.tick();
	}
	
	@Override
	public void callback() {
		if(disabled)
			return;
		
		try {
			if(PlantButton.selected_id == -1)
				return;
			
			PlantButton pb = PlantButton.plantButtons.get(PlantButton.selected_id);
			
			if(pb instanceof DigButton && placeable != null) {
				placeable.kill();
				return;
			}
			
			if(placeable != null)
				return;
			
			if(pb.cooldown > 0)
				return;
			
			Blueprint blueprint = pb.blueprint;
			
			if(blueprint == null)
				return;
			
			int cost = blueprint.cost;
			if(level.sunsBalance < cost)
				return;
			
			pb.cooldown = pb.blueprint.cooldown;
			
			level.sunsBalance -= cost;
			Constructor<Placeable> cons = blueprint.clazz.getConstructor(int.class, int.class, Level.class, int.class, int.class);
			placeable = cons.newInstance(x, y - 20, level, row, col);
			
			PlantButton.selected_id = -1;
			level.showPreview = false;
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
