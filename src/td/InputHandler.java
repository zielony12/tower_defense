package td;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

public class InputHandler implements KeyListener, MouseListener, MouseMotionListener {
	public class Key {
		public int presses, absorbs;
		public boolean down, clicked;
		
		public Key() {
			keys.add(this);
		}
		
		public void toggle(boolean pressed) {
			if(pressed != down)
				down = pressed;
			if(pressed)
				presses++;
		}
		
		public void tick() {
			if(absorbs < presses) {
				absorbs++;
				clicked = true;
			} else
				clicked = false;
		}
	}
	
	private Game game;
	
	public List<Key> keys = new ArrayList<Key>();
	
	public Key up = new Key();
	public Key down = new Key();
	public Key left = new Key();
	public Key right = new Key();
	public Key fire = new Key();
	
	public void releaseAll() {
		for(int i = 0; i < keys.size(); i++) {
			keys.get(i).down = false;
		}
	}
	
	public void tick() {
		for(int i = 0; i < keys.size(); i++) {
			keys.get(i).tick();
		}
	}
	
	public InputHandler(Game game) {
		this.game = game;
		game.addKeyListener(this);
		game.addMouseListener(this);
		game.addMouseMotionListener(this);
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		toggle(e, true);
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		toggle(e, false);
	}
	
	public void toggle(KeyEvent e, boolean pressed) {
		int code = e.getKeyCode();
		switch(code) {
		case KeyEvent.VK_UP:
			up.toggle(pressed);
			break;
		case KeyEvent.VK_DOWN:
			down.toggle(pressed);
			break;
		case KeyEvent.VK_LEFT:
			left.toggle(pressed);
			break;
		case KeyEvent.VK_RIGHT:
			right.toggle(pressed);
			break;
		case KeyEvent.VK_SPACE:
			fire.toggle(pressed);
			break;
		}
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
	}
	
	@Override	
	public void mouseExited(MouseEvent e) {
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		game.clicked(e.getX(), e.getY());
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		game.moved(e.getX(), e.getY());
	}
}
