package td.gfx;

import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Gfx {
	public static Font font;
	public static Font font_s;
	public static Font font_xs;
	
	static {
		try {
			font = Font.createFont(Font.TRUETYPE_FONT, Gfx.class.getResourceAsStream("/font.ttf")).deriveFont(48f);
			font_s = Font.createFont(Font.TRUETYPE_FONT, Gfx.class.getResourceAsStream("/font_b.ttf")).deriveFont(24f);
			font_xs = Font.createFont(Font.TRUETYPE_FONT, Gfx.class.getResourceAsStream("/font_b.ttf")).deriveFont(18f);
		} catch(IOException | FontFormatException e) {
			e.printStackTrace();
		}
	}
	public static final BufferedImage lawn_mower = load("/lawn_mower.png");
	public static final BufferedImage zombie[] = {
			load("/zombie2.png"),
			load("/zombie1.png"),
			load("/zombie3.png")
	};
	public static final BufferedImage sun = load("/sun.png");
	public static final BufferedImage bg_day = load("/bg_day.png");
	public static final BufferedImage[] field = {
			load("/field_middle.png"),
			load("/field_disabled_middle.png")
	};
	public static final BufferedImage[] seam = {
			load("/seam_top.png"),
			load("/seam_bottom.png"),
	};
	public static final BufferedImage shovel = load("/shovel.png");
	public static final BufferedImage[] pacman = {
		load("/pacman1.png"),
		load("/pacman2.png"),
		load("/pacman3.png")
	};
	public static final BufferedImage[] shards = {
		load("/shard1.png"),
		load("/shard2.png"),
		load("/shard3.png")
	};
	public static final BufferedImage[] pea = {
			load("/pea1.png"),
			load("/pea2.png")
	};
	public static final BufferedImage peashooter = load("/peashooter.png");
	public static final BufferedImage[] wallnut = {
			load("/wallnut1.png"),
			load("/wallnut2.png"),
			load("/wallnut3.png"),
			load("/wallnut4.png")
	};
	public static final BufferedImage sunflower = load("/sunflower.png");
	public static final BufferedImage blood = load("/blood.png");
	public static final BufferedImage selection_overlay = load("/selection_overlay.png");
	public static final BufferedImage title = load("/title.png");
	public static final BufferedImage[] play_btn = {
			load("/play_btn.png"),
			load("/play_btn2.png")
	};
	public static final BufferedImage logo = load("/logo.png");
	
	public static BufferedImage load(String path) {
		try {
			BufferedImage image_tmp = ImageIO.read(Gfx.class.getResourceAsStream(path));
			
			int width = image_tmp.getWidth();
			int height = image_tmp.getHeight();
			
			BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
			
			for(int x = 0; x < width; x++) {
				for(int y = 0; y < height; y++) {
					int color = image_tmp.getRGB(x, y);
					if(color == 0xFFFF00FF)
						image.setRGB(x, y, 0x00FFFFFF);
					else
						image.setRGB(x, y, color);
				}
			}
			
			return image;
		} catch(IOException e) { 
			throw new RuntimeException(e);
		}
	}
	
	public static BufferedImage dim(BufferedImage obrazek) {
	    BufferedImage dimmed = new BufferedImage(obrazek.getWidth(), obrazek.getHeight(), BufferedImage.TYPE_INT_ARGB);
	    Graphics2D g2d = dimmed.createGraphics();
	    g2d.drawImage(obrazek, 0, 0, null);
	    
	    for (int y = 0; y < obrazek.getHeight(); y++) {
	        for (int x = 0; x < obrazek.getWidth(); x++) {
	            int pixel = obrazek.getRGB(x, y);
	            int alpha = (pixel >> 24) & 0xFF;
	            int red = (pixel >> 16) & 0xFF;
	            int green = (pixel >> 8) & 0xFF;
	            int blue = pixel & 0xFF;
	            
	            // dim by 50%; TODO: `double percent`
	            red = red * 2 / 3;
	            green = green * 2 / 3;
	            blue = blue * 2 / 3;
	            
	            int newPixel = (alpha << 24) | (red << 16) | (green << 8) | blue;
	            
	            dimmed.setRGB(x, y, newPixel);
	        }
	    }
	    
	    g2d.dispose();
	    return dimmed;
	}

	public static BufferedImage dim(BufferedImage obrazek, int color) {
	    BufferedImage mixed = new BufferedImage(obrazek.getWidth(), obrazek.getHeight(), BufferedImage.TYPE_INT_ARGB);
	    
	    for (int y = 0; y < obrazek.getHeight(); y++) {
	        for (int x = 0; x < obrazek.getWidth(); x++) {
	            int pixel = obrazek.getRGB(x, y);
	            
	            int newPixel = (pixel & 0xFF000000) | ((pixel & 0x00FFFFFF) & color);
	            
	            mixed.setRGB(x, y, newPixel);
	        }
	    }
	    
	    return mixed;
	}
	
	public static BufferedImage brighten(BufferedImage image, int color) {
	    BufferedImage res = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
	    Graphics2D g2d = res.createGraphics();
	    g2d.drawImage(image, 0, 0, null);
	    
	    for (int y = 0; y < image.getHeight(); y++) {
	        for (int x = 0; x < image.getWidth(); x++) {
	            int pixel = image.getRGB(x, y);
	            int alpha = (pixel >> 24) & 0xFF;
	            int red = (pixel >> 16) & 0xFF;
	            int green = (pixel >> 8) & 0xFF;
	            int blue = pixel & 0xFF;
	            
	            red = Math.max(0, Math.min(255, (red + ((color >> 16) & 0xFF))));
	            green = Math.max(0, Math.min(255, (green + ((color >> 8) & 0xFF))));
	            blue = Math.max(0, Math.min(255, (blue + (color & 0xFF))));
	            
	            int newPixel = (alpha << 24) | (red << 16) | (green << 8) | blue;
	            
	            res.setRGB(x, y, newPixel);
	        }
	    }
	    
	    g2d.dispose();
	    return res;
	}
	
	public static BufferedImage addTransparency(BufferedImage image, int a) {
	    BufferedImage res = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
	    
	    for (int y = 0; y < image.getHeight(); y++) {
	        for (int x = 0; x < image.getWidth(); x++) {
	            int pixel = image.getRGB(x, y);
	            
	            int alpha = (pixel >> 24) & 0xFF;
	            int red = (pixel >> 16) & 0xFF;
	            int green = (pixel >> 8) & 0xFF;
	            int blue = pixel & 0xFF;
	            
	            alpha = Math.max(0, alpha - a);
	            
	            int newPixel = (alpha << 24) | (red << 16) | (green << 8) | blue;
	            
	            res.setRGB(x, y, newPixel);
	        }
	    }
	    
	    return res;
	}

}
